import Uploader from './Uploader.vue';

module.exports = {
  install: function (Vue, options) {
    Vue.component('cnt-uploader', Uploader);
  }
};